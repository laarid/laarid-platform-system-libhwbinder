cmake_minimum_required(VERSION 3.9)
cmake_policy(VERSION 3.9)
project(dummy LANGUAGES CXX)
find_package(LaaridHwBinder REQUIRED)
add_executable(dummy-cmake dummy.cpp)
target_link_libraries(dummy-cmake Laarid::LaaridHwBinder)
